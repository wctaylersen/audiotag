# Development Notes

These notes record the steps I am taking through development of the project. They are meant both as a reminder to myself of what I've done throughout the process and also as a reference to other developers who may be interested in a beginner's perspective on creating an application.

My intention is to first create a minimal functional example, and then build up to the additional features included in many templates. 