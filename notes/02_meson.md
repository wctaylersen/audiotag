# Meson

To use the Meson build system, each directory that is included needs a `meson.build` file to describe the build process. The minimal build file for the root directory of the project is shown below.

```
Filename: meson.build

project('audiotag', 'rust')
```

The `project` function should be provided with the project name and the programming language(s) used. If multiple languages are used, the language parameter should be a list. However, as of meson version 0.40.0, the language parameter is optional. 

While the minimal example works, it is good to include some additional information, such as the project version, the required Meson version, and possible other options. 

```
project(
	'audiotag', 
	'rust', 
    version: '0.1.0',
    meson_version: '>= 0.60.2',
    default_options: [ 
		'warning_level=2',
    ],
)
```

While this is a valid `meson.build` file, it does not actually do anything. To actually build our project, we need to tell Meson what to do. Since we are using Cargo, we essentially just want to tell Meson how to use Cargo. This can be done with the following:

```
cargo = find_program('cargo')
run_command(
	cargo,
	'build',
	'--target-dir=@0@'.format(meson.current_build_dir())
)
```

We tell Meson to find the Cargo program, so that it can use it, and then we tell Meson to run `cargo build --target-dir=<meson_build_dir>`.

With this, the complete contents of `meson.build` are 
```
project(
	'audiotag',
	'rust',
	version: '0.1.0',
    meson_version: '>= 0.60.2',
 	default_options: [
		'warning_level=2',
    ],
)

cargo = find_program('cargo')
run_command(
	cargo,
	'build',
	'--target-dir=@0@'.format(meson.current_build_dir())
)
```

For our currently minimal application, this is sufficient. But we will begin to add complexity as the project grows. 

Additional documentation can be found at [The Meson Website](https://mesonbuild.com/). 