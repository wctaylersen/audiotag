# Setup

The setup and development sequence primarily follows the documentation found in the following resources

* [The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html)  
* [GUI development with Rust and GTK 4](https://gtk-rs.org/gtk4-rs/stable/latest/book/introduction.html)

## Installing Dependencies

1. Install Rust  
`curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh`  
2. Install GTK4  
`sudo apt install libgtk-4-dev build-essential`  

## Project

First, use the Cargo tool provided with the Rust installation to create the project
```
cargo new audiotag
```

Then, update `Cargo.toml` to include the `gtk4` dependency


```
Filename: Cargo.toml

[dependencies]
gtk = { version = "0.3", package = "gtk4" }
```

Update `src/main.rs` to create the minimal application.
```
Filename: src/main.rs

use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};

fn main() {
    // Create a new application
    let app = Application::builder()
        .application_id("com.gitlab.wctaylersen.AudioTag")
        .build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run();
}

fn build_ui(app: &Application) {
    // Create a window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("AudioTag")
        .build();

    // Present window
    window.present();
}
```

Run the program
```
cargo run
```